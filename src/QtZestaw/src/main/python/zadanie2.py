from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QWidget, QLabel, QGridLayout, QApplication

import sys

counter = 0


class ExtendedQLabel(QLabel):
    enable = 0

    def __init(self, parent):
        super().__init__(parent)

    clicked = pyqtSignal()

    def mousePressEvent(self, ev):
        global counter
        if self.enable == 0:
            if counter % 2 == 0:
                pix = QPixmap('cancel.png')
                self.setPixmap(pix)
                self.enable += 1
                counter += 1
            else:
                pix = QPixmap('checked.png')
                self.setPixmap(pix)
                counter += 1
                self.enable += 1


if __name__ == '__main__':
    app = QApplication([])

    w = QWidget()
    grid = QGridLayout(w)

    for i in range(3):
        for j in range(3):
            label = ExtendedQLabel()
            pix = QPixmap('empty.png')
            label.setPixmap(pix)
            label.clicked.connect(lambda: print('clicked'))
            grid.addWidget(label, i, j)

    w.show()
    sys.exit(app.exec_())
