from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QMainWindow, QPushButton, QWidget, QLabel, QGridLayout, QVBoxLayout, QHBoxLayout, \
    QApplication

import sys

if __name__ == "__main__":
    app = QApplication([])

    w = QWidget()
    grid = QGridLayout(w)
    label = QLabel()
    pix = QPixmap('spotify.png')
    label.setPixmap(pix)

    for i in range(3):
        for j in range(3):
            label = QLabel()
            pix = QPixmap('spotify.png')
            label.setPixmap(pix)
            grid.addWidget(label, i, j)

    w.show()
    sys.exit(app.exec_())
