import unittest

from src.zestaw6.point import *


class TestPoint(unittest.TestCase):

    def setUp(self):
        self.zero = [0, 1]

    def test_cmp(self):
        self.assertTrue(Point(1, 2) == Point(1, 2))
        self.assertTrue(Point(1, 3) != Point(1, 2))

    def test_add(self):
        self.assertEqual(Point(1, 2) + Point(1, 2), Point(2, 4))

    def test_sub(self):
        self.assertEqual(Point(1, 2) - Point(1, 2), Point(0, 0))

    def test_mul(self):
        self.assertEqual(Point(5, 1) * Point(-2, 4), -6)

    def test_length(self):
        self.assertEqual(Point(4, 3).length(), 5)

    def test_cross(self):
        self.assertEqual(Point(3, 2).cross(Point(3, 2)), 0)

    def tearDown(self): pass


if __name__ == '__main__':
    unittest.main()
