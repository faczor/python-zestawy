from random import randint


class RandomQueue:

    def __init__(self, size=10):
        self.items = size * [None]
        self.n = 0
        self.size = size

    def is_empty(self):
        return self.n == 0

    def is_full(self):
        return self.size == self.n

    def insert(self, item):
        if self.n == self.size:
            raise Exception("Full stack")
        self.items[self.n] = item
        self.n += 1

    def remove(self):  # zwraca losowy element
        if self.n < 0:
            raise Exception("Empty stack")
        tmp = randint(0, self.n - 1)
        x = self.items[tmp]
        #while tmp < self.size - 1:
        #    self.items[tmp] = self.items[tmp + 1]
        #    tmp += 1
        self.items[tmp] = self.items[self.n - 1]
        self.n -= 1
        return x

    def clear(self):  # czyszczenie listy
        self.items = self.size * [None]
        self.n = 0


que = RandomQueue(6)
que.insert(3)
que.insert(5)
que.insert(7)
que.insert(9)
que.insert(11)
que.insert(13)
print(que.remove())
print(que.remove())
print(que.remove())
print(que.remove())


