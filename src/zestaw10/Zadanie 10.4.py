import unittest

from src.zestaw10.queue import Queue


class TestStack(unittest.TestCase):

    def setUp(self):
        self.queue = Queue(2)

    def test_is_empty(self):
        self.assertTrue(self.queue.is_empty())
        self.queue.put(5)
        self.assertFalse(self.queue.is_empty())

    def test_put(self):
        self.queue.put(1)
        self.queue.put(2)
        self.assertFalse(self.queue.is_empty())

    def test_get(self):
        self.queue.put(1)
        self.queue.put(2)
        self.assertEqual(self.queue.get(), 1)

    def test_is_full(self):
        self.queue.put(1)
        self.queue.put(2)
        self.assertTrue(self.queue.is_full)

    def tearDown(self):
        self.queue = None


if __name__ == '__main__':
    unittest.main()