import unittest

from src.zestaw10.stack import Stack


class TestStack(unittest.TestCase):

    def setUp(self):
        self.firstStack = Stack(2)

    def test_is_empty(self):
        self.assertTrue(self.firstStack.is_empty())
        self.firstStack.push(5)
        self.assertFalse(self.firstStack.is_empty())

    def test_push(self):
        self.firstStack.push(1)
        self.firstStack.push(2)
        self.assertFalse(self.firstStack.is_empty())

    def test_pop(self):
        self.firstStack.push(1)
        self.firstStack.push(2)
        self.assertEqual(self.firstStack.pop(), 2)

    def test_is_full(self):
        self.firstStack.push(1)
        self.firstStack.push(2)
        self.assertTrue(self.firstStack.is_full)

    def tearDown(self):
        self.firstStack = None


if __name__ == '__main__':
    unittest.main()