from memorize.memorize import memorize


@memorize()
def p(i, j):
    if i == 0 and j == 0:
        return 0.5
    elif i > 0 and j == 0:
        return 0.0
    elif i == 0 and j > 0:
        return 1.0
    else:
        return 0.5 * (p(i - 1, j) + p(i, j - 1))


print(p(30, 25))
