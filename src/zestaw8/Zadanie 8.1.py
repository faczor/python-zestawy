def solve1(a, b, c):
    m = -1 * (a / b)
    b = -1 * (c / b)
    return m, b


m, b = solve1(6, -3, -15)
print("Rozwiązanie 6x - 3y - 15 = 0, w postaci y = mx + b:")
print("y =", m, "x +", b)
