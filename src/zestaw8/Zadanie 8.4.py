from math import sqrt


def heron(a, b, c):
    try:
        p = (a + b + c)/2
        return sqrt(p * (p - a) * (p - b) * (p - c))
    except ValueError:
        print("Invalid input")


print(heron(2, 3, 4))
