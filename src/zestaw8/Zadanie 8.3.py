import random


def calc_pi(n):
    k, i = 0, 1
    while i <= n:
        x = random.uniform(0, 1)
        y = random.uniform(0, 1)
        if pow(x, 2) + pow(y, 2) <= 1:
            k += 1
        i += 1
    return 4 * k / n


print(calc_pi(100))
