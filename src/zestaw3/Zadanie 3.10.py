rom_val = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
int_val = 0
rom = "MMMCMLXXXVI"
for i in range(len(rom)):
    if i > 0 and rom_val[rom[i]] > rom_val[rom[i - 1]]:
        int_val += rom_val[rom[i]] - 2 * rom_val[rom[i - 1]]
    else:
        int_val += rom_val[rom[i]]
print("Rom przetłumaczone na int", int_val)
#Inne sposoby jakie mi przychodzą do głowy:
# wykorzystanie petli switch badz kilka if