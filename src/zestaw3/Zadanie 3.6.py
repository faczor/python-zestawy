def func(x, y):
    if x == 0 or y == 0:
        return ''
    lista1, lista2, lista3 = '', '', ''
    for j in range(y):
        lista1 += '+---'
        lista2 += "|   "
    lista1 += '+'
    lista2 += '|'
    final = lista1
    for i in range(x):
        final += '\n' + lista2 + '\n' + lista1
    return final


print(func(1, 5))
