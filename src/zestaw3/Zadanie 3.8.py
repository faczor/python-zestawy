lista1 = [3, "Auto", 3.25, "Python", 3]
lista2 = ["Python", 5, "Trajka", 3.30, "Java", 3, 3]
lista3 = []
for x in lista1:
    if x in lista2 and x not in lista3:
        lista3.append(x)
print("Lista elementów występujących jednocześnie w obu sekwencjach: ", lista3)

lista4 = []
for x in lista1:
    if x not in lista3:
        lista3.append(x)
for x in lista2:
    if x not in lista3:
        lista3.append(x)
print("Lista elementów wszystkich elementow z obu sekwencji ", lista3)
