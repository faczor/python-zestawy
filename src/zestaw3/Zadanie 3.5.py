while True:
    x = input("Podaj długość miarki:")
    if not x.isdigit():
        print("To nie jest liczba!")
    else:
        break
x = int(x)
lista1, lista2 = [], []
for i in range(x + 1):
    if i == x:
        lista1.append("|")
        lista2.append(i)
    else:
        lista1.append("|....")
        lista2.append(i)
        lista2.append("    ")
p, j = "".join([str(i) for i in lista1]), "".join([str(i) for i in lista2])


s = p + '\n' + j
print(s)
