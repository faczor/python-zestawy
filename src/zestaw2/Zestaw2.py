line = "Czesc jestem GvR Sebek \nUcze sie Pythona"
word = "wyraz"
print(line)
# Zadanie 2.10
print("----------ZAD 2.10--------")
words = line.split()
count = 0
for x in words:
    count += 1
print("Ilosc wyrazow w line:", count)
print("--------------------------\n")

# Zadanie 2.11
print("----------ZAD 2.11--------")
word2 = ""
wordLen = len(word)
for i in range(wordLen):
    if i < wordLen - 1:
        word2 += word[i]+'_'
    else:
        word2 += word[i]
print(word2)
print("--------------------------\n")

# Zadanie 2.12
print("----------ZAD 2.12--------")
wyrazPom = ""
for x in words:
    wyrazPom += x[0]
print("Wyraz z pierwszych znaków", wyrazPom)

wyrazPom = ""
for x in words:
    wyrazPom += x[len(x) - 1]
print("Wyraz z ostatnich znaków", wyrazPom)
print("--------------------------\n")

# Zadanie 2.13
print("----------ZAD 2.13--------")
counter = 0
for x in words:
    counter += len(x)
print("Laczna dlugosc wyrazow napisie line: ", counter)
print("--------------------------\n")

# Zadanie 2.14
print("----------ZAD 2.14--------")
longestWord = ""
for x in words:
    if len(x) > len(longestWord):
        longestWord = x
print("Najdluzszy wyraz to: ", longestWord, " o dlugosci: ", len(longestWord))
print("--------------------------\n")

# Zadanie 2.15
print("----------ZAD 2.15--------")
list1 = [1, 3, 6, 7, 20]
print("Polaczona lista:", "".join([str(i) for i in list1]))
print("--------------------------\n")

# Zadanie 2.16
print("----------ZAD 2.16--------")

tmpWords = [w.replace('GvR', 'Guido van Rossum') for w in words]
print(" ".join([str(i) for i in tmpWords]))
print("--------------------------\n")

# Zadanie 2.17
print("----------ZAD 2.17--------")
list2 = sorted(line.split())
print("Line posortowane alfabetycznie: ", " ".join([str(i) for i in list2]))

list2 = sorted(line.split(), key=len)
print("Line posortowane biorac pod uwage dlugosc: ", " ".join([str(i) for i in list2]))
print("--------------------------\n")

# Zadanie 2.18
print("----------ZAD 2.18--------")
liczba = 51080515000
counter = 0
strLiczba = str(liczba)
for x in strLiczba:
    if x == "0":
        counter += 1
print("Liczba zer wynosi: ", counter)
print("--------------------------\n")

# Zadanie 2.19
print("----------ZAD 2.19--------")
list3 = [1, 22, 333, 4, 55, 666]
list4 = (' '.join(str(x) for x in list3)).split()
list5 = []
print(list4)
for x in list4:
    list5.append(x.zfill(3))
print("Lista z dopełnieniem:", list5)
print("--------------------------\n")