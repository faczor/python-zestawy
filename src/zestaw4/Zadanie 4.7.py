lista1 = []


def sum_seq(sequence):
    for x in sequence:
        if isinstance(x, int):
            lista1.append(x)
        else:
            for y in x:
                if isinstance(y, int):
                    lista1.append(y)
                else:
                    sum_seq(y)
    return lista1


seq = [1, [2, [3, [4, [5, 6]]]]]
lista2 = sum_seq(seq)
print("Wynik: ", lista2)
