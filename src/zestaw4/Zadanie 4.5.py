def odwracanie(L, left, right):
    i = right - left
    for x in range(int(i/2) + 1):
        L[left + x], L[right - x] = L[right - x], L[left + x]


def odwracanieRek(L, left, right):
    L[left], L[right] = L[right], L[left]
    if left < right:
        odwracanieRek(L, left + 1, right - 1)


L = [1, 2, 3, 4, 5, 6, 7, 8, 9]
L2 = [1, 2, 3, 4, 5, 6, 7, 8, 9]

odwracanie(L, 3, 7)
print("Odwracanie iteracyjnie: ", L)
odwracanie(L2, 3, 7)
print("Odwracanie rekurencyjnie: ", L)

