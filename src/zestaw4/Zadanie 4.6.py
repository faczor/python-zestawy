def sum_seq(sequence):
    lista1 = []
    tmp = 0
    for x in sequence:
        if isinstance(x, int):
            lista1.append(x)
        else:
            sum_help(x, lista1, tmp)
    return lista1


def sum_help(sequence, lista1, tmp):
    for y in sequence:
        if isinstance(y, int):
            tmp += y
        else:
            for p in y:
                if isinstance(p, int):
                    tmp += p
                else:
                    sum_help(p, lista1, tmp)
    print(tmp)
    lista1.append(tmp)


seq = [1, (2, 3), [], [4, (5, 6, 7)], 8, [9]]

lista2 = sum_seq(seq)
print("Wynik: ", lista2)
