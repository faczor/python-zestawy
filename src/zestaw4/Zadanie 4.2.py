def funkcjaPythonowaRysujacaplusminus(y):
    if y == 0:
        return '+'
    else:
        return '+---' + funkcjaPythonowaRysujacaplusminus(y - 1)


def funkcjaPythonowaRysujaceScianyIodstepy(y):
    if y == 0:
        return '|'
    else:
        return '|   ' + funkcjaPythonowaRysujaceScianyIodstepy(y - 1)


def funkcjaPythonowaPoProstuRysujaca(x, plusyiminusy, scianyiodstepy):
    if x == 0:
        return plusyiminusy
    else:
        return plusyiminusy + '\n' + scianyiodstepy + '\n' + funkcjaPythonowaPoProstuRysujaca(x - 1, plusyiminusy, scianyiodstepy)


def funkcjaPythonowaRysujacaKwadraty(x,y):
    if x == 0 or y == 0:
        return ''
    plusyiminusy = funkcjaPythonowaRysujacaplusminus(y)
    scianyiodstepy = funkcjaPythonowaRysujaceScianyIodstepy(y)
    return funkcjaPythonowaPoProstuRysujaca(x, plusyiminusy, scianyiodstepy)


print(funkcjaPythonowaRysujacaKwadraty(5, 4))
