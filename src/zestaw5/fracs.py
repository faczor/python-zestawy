def add_frac(frac1, frac2):
    tmp1, tmp2, tmp1[0], tmp1[1], tmp2[0], tmp2[1] = [0, 0], [0, 0], frac1[0], frac1[1], frac2[0], frac2[1]
    if frac1[1] != frac2[1]:
        frac1[1] *= tmp2[1]
        frac2[1] *= tmp1[1]
        frac1[0] *= tmp2[1]
        frac2[0] *= tmp1[1]
    frac = [0, 0]
    frac[0] = frac1[0] + frac2[0]
    frac[1] = frac1[1]
    return frac


def sub_frac(frac1, frac2):
    tmp1, tmp2, tmp1[0], tmp1[1], tmp2[0], tmp2[1] = [0, 0], [0, 0], frac1[0], frac1[1], frac2[0], frac2[1]
    if frac1[1] != frac2[1]:
        frac1[1] *= tmp2[1]
        frac2[1] *= tmp1[1]
        frac1[0] *= tmp2[1]
        frac2[0] *= tmp1[1]
    frac = [0, 0]
    frac[0] = frac1[0] - frac2[0]
    frac[1] = frac1[1]
    return frac


def mul_frac(frac1, frac2):
    frac = [0, 0]
    frac[0] = frac1[0] * frac2[0]
    frac[1] = frac1[1] * frac2[1]
    return frac


def div_frac(frac1, frac2):
    frac = [0, 0]
    frac[0] = frac1[0] * frac2[1]
    frac[1] = frac1[1] * frac2[0]
    return frac


def if_zero(frac):
    if frac[0] == 0:
        return True
    else:
        return False


def cmp_frac(frac1, frac2):
    tmp1, tmp2, tmp1[0], tmp1[1], tmp2[0], tmp2[1] = [0, 0], [0, 0], frac1[0], frac1[1], frac2[0], frac2[1]
    if frac1[1] != frac2[1]:
        frac1[1] *= tmp2[1]
        frac2[1] *= tmp1[1]
        frac1[0] *= tmp2[1]
        frac2[0] *= tmp1[1]

    if frac1[0] < frac2[0]:
        return -1
    elif frac1[0] == frac2[0]:
        return 0
    else:
        return 1


def frac2float(frac):
    return frac[0] / frac[1]


def is_positive(frac):
    if frac[0] < 0:
        return False
    else:
        return True

