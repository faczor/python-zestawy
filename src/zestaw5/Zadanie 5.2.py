import unittest

from src.zestaw5.fracs import *


class TestFractions(unittest.TestCase):

    def setUp(self):
        self.zero = [0, 1]

    def test_add_frac(self):
        self.assertEqual(add_frac([1, 2], [1, 3]), [5, 6])

    def test_sub_frac(self):
        self.assertEqual(sub_frac([5, 2], [1, 3]), [13, 6])

    def test_mul_frac(self):
        self.assertEqual(mul_frac([2, 3], [4, 3]), [8, 9])

    def test_div_frac(self):
        self.assertEqual(div_frac([2, 3], [4, 3]), [6, 12])

    def test_is_positive(self): 
        self.assertEqual(is_positive([-1, 2]), False)

    def test_is_zero(self):
        self.assertEqual(if_zero([0, 3]), True)

    def test_cmp_frac(self):
        self.assertEqual(cmp_frac([3, 9], [1, 2]), -1)

    def test_frac2float(self):
        self.assertEqual(frac2float([1, 2]), 0.5)

    def tearDown(self): pass


if __name__ == '__main__':
    unittest.main()