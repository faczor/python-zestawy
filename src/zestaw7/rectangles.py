from src.zestaw6.point import Point


class Rectangle:
    """Klasa reprezentująca prostokąt na płaszczyźnie."""

    def __init__(self, x1, y1, x2, y2):
        self.pt1 = Point(x1, y1)
        self.pt2 = Point(x2, y2)

    def __str__(self):  # "[(x1, y1), (x2, y2)]"
        return '[({}, {}), ({}, {})]'.format(self.pt1.x, self.pt1.y, self.pt2.x, self.pt2.y)

    def __repr__(self):  # "Rectangle(x1, y1, x2, y2)"
        return 'Rectangle[({}, {}), ({}, {})]'.format(self.pt1.x, self.pt1.y, self.pt2.x, self.pt2.y)

    def __eq__(self, other):  # obsługa rect1 == rect2
        try:
            if self.pt1.x == other.pt1.x \
                    and self.pt1.y == other.pt1.y \
                    and self.pt2.x == other.pt2.x \
                    and self.pt2.y == other.pt2.y:
                return True
            else:
                return False
        except ValueError:
            print("Invalid input")

    def __ne__(self, other):  # obsługa rect1 != rect2
        try:
            return not self == other
        except ValueError:
            print("Invalid input")

    def center(self):  # zwraca środek prostokąta
        try:
            return Point((self.pt1.x + self.pt2.x) / 2, (self.pt1.y + self.pt2.y) / 2)
        except ValueError:
            print("Invalid input")

    def area(self):  # pole powierzchni
        try:
            return (self.pt1.x - self.pt2.x) * (self.pt1.y - self.pt2.y)
        except ValueError:
            print("Invalid input")

    def move(self, x, y):  # przesunięcie o (x, y)
        try:
            return Rectangle(self.pt1.x + x, self.pt1.y + y, self.pt2.x + x, self.pt2.y + y)
        except ValueError:
            print("Invalid input")

    def intersection(self, other):  # część wspólna prostokątów
        try:
            return Rectangle(max(self.pt1.x, other.pt1.x),
                             min(self.pt1.y, other.pt1.y),
                             max(self.pt2.x, other.pt2.x),
                             min(self.pt2.y, other.pt2.y))
        except ValueError:
            print("Invalid input")

    def cover(self, other):  # prostąkąt nakrywający oba
        try:
            if self.pt1.x < other.pt1.x:
                x1 = self.pt1.x
            else:
                x1 = other.pt1.x
            if self.pt1.y > other.pt1.y:
                y1 = self.pt1.y
            else:
                y1 = other.pt1.y
            if self.pt2.x > other.pt2.x:
                x2 = self.pt2.x
            else:
                x2 = other.pt2.x
            if self.pt2.y < other.pt2.y:
                y2 = self.pt2.y
            else:
                y2 = other.pt2.y
            return Rectangle(x1, y1, x2, y2)
        except ValueError:
            print("Invalid input")

    def make4(self):
        try:
            lista1 = []
            for x in range(5):
                if x != 0:
                    lista1.append(Rectangle(self.pt1.x / x, self.pt1.y / x, self.pt2.x / x, self.pt2.y / x))
            return lista1
        except ValueError:
            print("Invalid input")
