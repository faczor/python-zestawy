import unittest

from src.zestaw7.circle import Circle


class TestCircle(unittest.TestCase):
    def test_cmp(self):
        self.assertTrue(Circle(2, 2, 2) == Circle(2, 2, 2))
        self.assertTrue(Circle(2, 2, 2) != Circle(2, 2, 3))

    def test_area(self):
        self.assertEqual(Circle(2, 3, 6).area(), 113.09733552923255)

    def test_move(self):
        self.assertEqual(Circle(2, 3, 6).move(2, 3), Circle(4, 6, 6))

    def test_cover(self):
        self.assertEqual(Circle(2, 2, 2).cover(Circle(5, 3, 1)), Circle(3.5, 2.5, 3))


if __name__ == '__main__':
    unittest.main()
