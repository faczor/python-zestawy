import unittest

from src.zestaw6.point import Point
from src.zestaw7.rectangles import Rectangle


class TestRectangle(unittest.TestCase):

    def test_cmp(self):
        self.assertTrue(Rectangle(2, 3, 4, 5) == Rectangle(2, 3, 4, 5))
        self.assertTrue(Rectangle(2, 3, 4, 5) != Rectangle(2, 3, 4, 8))

    def test_center(self):
        self.assertEqual(Rectangle(1, 2, 5, 8).center(), Point(3, 5))

    def test_area(self):
        self.assertEqual(Rectangle(1, 2, 5, 8).area(), 24)

    def test_move(self):
        self.assertEqual(Rectangle(1, 2, 5, 8).move(2, 3), Rectangle(3, 5, 7, 11))

    def test_cover(self):
        self.assertEqual(Rectangle(1, 3, 3, 1).cover(Rectangle(2, 6, 5, 2)), Rectangle(1, 6, 5, 1))


if __name__ == '__main__':
    unittest.main()
