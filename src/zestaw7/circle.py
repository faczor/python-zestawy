import math

from src.zestaw6.point import Point


class Circle:
    """Klasa reprezentująca okręgi na płaszczyźnie."""

    def __init__(self, x, y, radius):
        if radius < 0:
            raise ValueError("promień ujemny")
        self.pt = Point(x, y)
        self.radius = radius

    def __repr__(self):        # "Circle(x, y, radius)"
        return 'Circle({}, {}, {})'.format(self.pt.x, self.pt.y, self.radius)

    def __eq__(self, other):
        try:
            return self.pt == other.pt and self.radius == other.radius
        except ValueError:
            print("Invalid input")

    def __ne__(self, other):
        try:
            return not self == other
        except ValueError:
            print("Invalid input")

    def area(self):            # pole powierzchni
        try:
            return math.pi * pow(self.radius, 2)
        except ValueError:
            print("Invalid input")

    def move(self, x, y):      # przesuniecie o (x, y)
        try:
            return Circle(self.pt.x + x, self.pt.y + y, self.radius)
        except ValueError:
            print("Invalid input")

    def cover(self, other):    # okrąg pokrywający oba
        try:
            return Circle((self.pt.x + other.pt.x)/2, (self.pt.y + other.pt.y)/2, self.radius + other.radius)
        except ValueError:
            print("Invalid input")
